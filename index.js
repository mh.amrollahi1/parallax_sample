"use strict";

const firstPage = document.querySelector(".firstpage");
const secondPage = document.querySelector(".secondpage");
const thirdPage = document.querySelector(".thirdpage");

const mainTitle = document.querySelector(".firstpage h1");
const background = document.querySelector(".background");
const middleground = document.querySelector(".middleground");
const foreground = document.querySelector(".foreground");
const spans = document.querySelectorAll(".secondpage h3 span");
const secondPageHeading = secondPage.querySelector("h3.heading");
// const progressLine = thirdPage.querySelector(".thirdpage .progress-line");
const lego = secondPage.querySelector("img");

const FIRST_PAGE_MAX_SCROLL = 500;
const FIRST_TRANS_MIN = 400;
const FIRST_TRANS_MAX = 600;
const THIRD_PAGE_START = 1800;
const JAVATITLE_MAX_SCROLL = 350;
const SECONDPAGE_BR = FIRST_TRANS_MAX + JAVATITLE_MAX_SCROLL;

thirdPage.style.opacity = 0;
let _width = window.innerWidth || document.documentElement.clientWidth || document.body.clientHeight;
let _height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

console.log('Width = ',_width);
console.log('Height = ',_height);

console.log('window.innerWidth = ',window.innerWidth);
console.log('window.innerHeight = ',window.innerHeight);
console.log('document.documentElement.clientHeight = ', document.documentElement.clientHeight);
console.log('document.body.clientHeight = ',document.body.clientHeight);
console.log('screen.width = ', screen.width);
console.log('screen.height = ',screen.height);
console.log('window.scree.availHeight = ',window.screen.availHeight);


document.addEventListener("scroll", function (event) {
    let scrollOffset = window.pageYOffset;

    if (scrollOffset <= FIRST_PAGE_MAX_SCROLL) {
        firstPage.style.opacity = 1;
        firstPage.hidden = false;

        let p = scrollOffset / FIRST_PAGE_MAX_SCROLL;
        mainTitle.style.transform = `scale(${1 + 0.9 * p})`;
        background.style.transform = `scale(${1 + 0.3 * p})`;
        middleground.style.transform = `scale(${1 + p})`;
        foreground.style.transform = `scale(${1 + 0.5 * p})`;
        // mainTitle.style.color = "red";
    }

    if (scrollOffset > FIRST_TRANS_MIN && scrollOffset < FIRST_TRANS_MAX) {
        // let op = (1 - scrollOffset / FIRST_PAGE_MAX_SCROLL) * 5;
        let op = scrollOffset / FIRST_TRANS_MAX;
        firstPage.style.opacity = 1 - op > 0.3 ? 1 - op : 0;
        secondPage.style.opacity = op;

        // if (op <= 0) {
        //     firstPage.hidden = true;
        // } else {
        //     firstPage.style.opacity = op;
        // }
    } else if (
        scrollOffset > FIRST_TRANS_MAX &&
        scrollOffset <= SECONDPAGE_BR
    ) {
        let ySecondPage = scrollOffset - FIRST_TRANS_MAX;
        let sevenOpcty = SECONDPAGE_BR / scrollOffset - 1;
        spans[0].style.transform = `translateY(${ySecondPage}px)`;
        spans[2].style.transform = `translateY(-${ySecondPage}px)`;
        spans[0].style.opacity = sevenOpcty;
        // progressLine.style.opacity = sevenOpcty;
    } else if (scrollOffset >= SECONDPAGE_BR) {
        let thirdScroll = scrollOffset - SECONDPAGE_BR;
        thirdPage.style.opacity = 1;
        app.style.transform = `translateY(-${thirdScroll}px)`;
    }
});
